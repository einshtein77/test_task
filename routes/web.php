<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['redirectTo'=>'none']);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function (){

    Route::post('/create_post', 'PostsController@create')->name('createPost');

    Route::post('/post_page', 'PostsController@index');

    Route::post('/get_comments', 'PostsController@get_comments');

    Route::get('/posts', 'PostsController@all_posts')->name('all_posts');

    Route::post('/comment_add', 'PostsController@comment_add')->name('comment_add');

});

