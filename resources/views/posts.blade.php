@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Text</th>
                        <th>Image</th>
                        <th>User</th>
                        <th>Date Time</th>
                        <th>Add a comment</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{$post['id']}}</td>
                            <td>{{$post['title']}}</td>
                            <td>{{$post['text']}}</td>
                            <td><img src="{{$post['image']}}" alt="" class="img-thumbnail"
                                     style="max-width: 80px;max-height: 80px;"></td>
                            <td>{{$post['user']['name']}}</td>
                            <td>{{$post['created_at']}}</td>
                            <td>
                                <form class="form-inline" method="post" action="{{route('comment_add')}}">
                                    @csrf
                                    <input type="hidden" value="{{$post['id']}}" name="post_id">
                                    <div class="form-group">
                                        <label for="comment" class="sr-only">Comment</label>
                                        <input name="comment" type="text" class="form-control" id="comment"
                                               placeholder="Comment" maxlength="100">
                                    </div>
                                    <button type="submit" class="btn btn-primary ml-2">Add Comment</button>
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="7" class="p-0">
                                <div id="accordion{{$post->id}}">
                                    <div class="card col-12 pl-0 pr-0">
                                        <div class="card-header" id="headingOne{{$post->id}}">
                                            <div class="mb-0 text-center">
                                                <button class="btn btn-link button" data-toggle="collapse"
                                                        data-target="#collapseOne{{$post->id}}" aria-expanded="true"
                                                        aria-controls="collapseOne{{$post->id}}" data-param="{{$post->id}}">{{$post->title}} all comments
                                                    <i class="fa fa-hand-o-down" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div id="collapseOne{{$post->id}}" class="collapse hide" aria-labelledby="headingOne{{$post->id}}"
                                             data-parent="#accordion{{$post->id}}">
                                            <div class="card-body">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <td>#</td>
                                                        <td>Name</td>
                                                        <td>Last Name</td>
                                                        <td>Comment</td>
                                                        <td>Date Time</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
                {{ $info->links() }}
            </div>
        </div>
    </div>


    <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
    <script src="{{asset('js/comments.js')}}"></script>
@endsection
