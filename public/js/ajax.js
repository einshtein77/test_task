function table_info(id,data) {
    return `
        <tr>
            <td>`+id+`</td>
            <td>`+data.title+`</td>
            <td>`+data.text+`</td>
            <td><img src="`+data.image+`" alt="" class="img-thumbnail" style="max-width: 80px;max-height: 80px;"></td>
            <td>`+data.user['name']+` `+data.user['last_name']+`</td>
            <td>`+data.created_at+`</td>
        </tr>
    `;
}

(function () {
    $.ajax({
        type: 'post',
        url: '/post_page',
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {
            for (var i=0; i<data.length;i++){
                var t=i+1;
                $('tbody').append(table_info(t,data[i]))
            }
        }
    })
})()
