function table_info(data) {
    return `
        <tr>
            <td>`+data.id+`</td>
            <td>`+data.user['name']+`</td>
            <td>`+data.user['last_name']+`</td>
            <td>`+data.comment+`</td>
            <td>`+data.created_at+`</td>
        </tr>
    `;
}


$(document).ready(function () {
    $('.button').click(function () {
        var id=$(this).data('param')
        tag='#collapseOne'+id
        $.ajax({
            type: 'post',
            url: '/get_comments',
            data: {
                'id': id,
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                $(tag+' tbody').empty()
                for (var i=0; i<data.length; i++){
                    $(tag+' tbody').append(table_info(data[i]))
                }
            }
        })
    })
})
