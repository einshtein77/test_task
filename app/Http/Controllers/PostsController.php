<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Posts;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{

    public function index()
    {
        $posts=Posts::take(5)->orderBy('created_at','DESC')->get();
        $info=[];

        foreach ($posts as $post){

            $post['user']=User::where('id',$post->user_id)->first();
            if ($post['image']==null){
                $post['image']='#';
            }
            unset($post['user_id'],$post['id']);
            array_push($info,$post);

        }

        return response()->json($info);
    }


    public function create(Request $request)
    {
        $input=$request->except('_token');

        $validator=Validator::make($input,[
            'title'=>'required|unique:posts|max:100',
            'text'=>'required',
            'image'=>'mimes:jpeg,jpg,png,gif|unique:posts|max:10000'
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input['user_id']=Auth::id();

        if ($request->hasFile('image')){

            $fail=$request->file ('image');
            $img=$fail->getClientOriginalName ();
            $input['image']='image/'.getdate(time())[0].'_new_post_image.'.pathinfo(storage_path().$img)['extension'];
            $fail->move (public_path ().'/image',$input['image']);


        }

        Posts::create($input);

        return redirect()->back()->with('status', 'Post Successful Added');

    }


    public function all_posts(Request $request)
    {

        $info=Posts::orderBy('created_at','DESC')->paginate(3);

        $posts=[];

        foreach ($info as $post){

            $post->load('comment');
            $post['user']=User::where('id',$post->user_id)->first();
            if ($post['image']==null){
                $post['image']='#';
            }
            unset($post['user_id']);
            array_push($posts,$post);

        }

        return view('posts',['posts'=>$posts,'info'=>$info]);
    }

    public function comment_add(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'comment'=>'required|max:100'
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $post=Posts::where('id',$request->post_id)->first();
        $input=$request->except(['_token','post_id']);
        $input['user_id']=Auth::id();
        $comment=Comments::create($input);
        $post->comment()->attach($comment);

        return redirect()->back();

    }

    public function get_comments(Request $request)
    {
        $post=Posts::where('id',$request->id)->first();
        $comments=$post->load('comment')->comment;

        foreach ($comments as $comment){
            $comment['user']= User::where('id',$comment->user_id)->first() ;
        }

        return response()->json($comments);
    }

}
