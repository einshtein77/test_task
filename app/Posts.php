<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable=['user_id','title','text','image'];

    public function comment(){
        return $this->belongsToMany(Comments::class);
    }
}
